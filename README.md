# Mumsnet Test

I have committed generated files, so downloading this repo and launching app/index.html in a browser should work. 

The site has also been deployed to http://mn.philbaker.me

## Build steps

- clone repository
- run ```npm install``` from root directory to install dependencies
- run ```gulp``` from root directory to launch local server with browsersync

## Browser testing

The site has been tested in:

- Firefox (latest macOS)
- Chrome (latest macOS)
- Safari (10 macOS)
- Safari iPhone 5 and iPad mini (10 iOS)
- Edge (Windows 10)
- IE11 (Windows 10)

## Todo

- IE11: fix small layout issues for articles (e.g. 'How pregnant am I?' title does not span full width of container). 
- Testing and bugfixes for Android
